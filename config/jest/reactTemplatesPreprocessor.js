const rt = require('react-templates');

module.exports = {
    process: function(src) {
        return rt.convertTemplateToReact(src, { modules: 'commonjs' })
    }
};
