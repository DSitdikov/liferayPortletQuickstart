package softrpo.quickstartportlet.spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SomeService {
    private RestTemplate restTemplate;

    @Value("${api-urls.some-url}")
    private String someUrl;

    @Autowired
    SomeService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getSomething() {
        return restTemplate.getForObject(someUrl + "/posts/1", String.class);
    }
}
