import { ActionCreator } from 'redux';
import { UiActionType } from '../ui-action-type';
import AppError from '../../../exceptions/app-error';
import CommonAction from '../../';

export const setBusy: ActionCreator<CommonAction> = (busy: boolean) => ({
    type: UiActionType.SET_BUSY,
    payload: busy
});

export const showError: ActionCreator<CommonAction> = (error: AppError) => ({
    type: UiActionType.SHOW_ERROR,
    payload: error
});
