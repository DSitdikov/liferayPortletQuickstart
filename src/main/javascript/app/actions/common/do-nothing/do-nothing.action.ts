import { Action, ActionCreator } from 'redux';
import { UiActionType } from '../../ui/ui-action-type';

export const doNothing: ActionCreator<Action> = () => ({ type: UiActionType.DO_NOTHING });