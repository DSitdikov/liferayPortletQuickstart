import AppNotification from '../ui/app-notification/app-notification';
import AppError from '../../exceptions/app-error';

export default interface UiState {
    common: {
        busy: boolean,
        error?: AppError,
        notification?: AppNotification
    };
}