import { combineReducers } from 'redux';
import uiCommonReducer from './ui-common/ui-common.reducer';

const uiReducer = combineReducers({
    common: uiCommonReducer,
});

export default uiReducer;